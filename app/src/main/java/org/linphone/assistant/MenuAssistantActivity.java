/*
 * Copyright (c) 2010-2019 Belledonne Communications SARL.
 *
 * This file is part of linphone-android
 * (see https://www.linphone.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.linphone.assistant;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import org.linphone.R;
import org.linphone.core.ConfiguringState;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.TransportType;
import org.linphone.core.tools.Log;
import org.linphone.settings.LinphonePreferences;

public class MenuAssistantActivity extends AssistantActivity implements TextWatcher {

    private static final int QR_CODE_ACTIVITY_RESULT = 1;
    private static final int CAMERA_PERMISSION_RESULT = 2;
    private TextView mFetchAndApply;
    private EditText mRemoteConfigurationUrl;
    private RelativeLayout mWaitLayout;

    private CoreListenerStub mListener;
    private TextView mLogin;
    private EditText mUsername, mPassword, mDomain, mDisplayName;
    private RadioGroup mTransport;

    private String defaultDomain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.assistant_menu);

        mLogin = findViewById(R.id.assistant_login);
        mLogin.setEnabled(false);
        mLogin.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        configureAccount();
                    }
                });

        mUsername = findViewById(R.id.assistant_username);
        mUsername.addTextChangedListener(this);
        //        mDisplayName = findViewById(R.id.assistant_display_name);
        // mDisplayName.addTextChangedListener(this);
        mPassword = findViewById(R.id.assistant_password);
        mPassword.addTextChangedListener(this);

        //        mDomain = findViewById(R.id.assistant_domain);
        //        mDomain.addTextChangedListener(this);

        //
        // defaultDomain = "172.24.54.6:5060";
        defaultDomain = "196.50.236.34";
        mTransport = findViewById(R.id.assistant_transports);

        TextView accountCreation = findViewById(R.id.account_creation);
        accountCreation.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent;
                        if (getResources().getBoolean(R.bool.isTablet)
                                || !getResources().getBoolean(R.bool.use_phone_number_validation)) {
                            intent =
                                    new Intent(
                                            MenuAssistantActivity.this,
                                            EmailAccountCreationAssistantActivity.class);
                        } else {
                            intent =
                                    new Intent(
                                            MenuAssistantActivity.this,
                                            PhoneAccountCreationAssistantActivity.class);
                        }
                        startActivity(intent);
                    }
                });

        TextView qrCode = findViewById(R.id.qr_code);
        qrCode.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkCameraPermissionForQrCode()) {
                            startActivityForResult(
                                    new Intent(
                                            MenuAssistantActivity.this,
                                            QrCodeConfigurationAssistantActivity.class),
                                    QR_CODE_ACTIVITY_RESULT);
                        }
                    }
                });

        mListener =
                new CoreListenerStub() {
                    @Override
                    public void onConfiguringStatus(
                            Core core, ConfiguringState status, String message) {
                        core.removeListener(mListener);
                        mWaitLayout.setVisibility(View.GONE);
                        mFetchAndApply.setEnabled(true);

                        if (status == ConfiguringState.Successful) {
                            LinphonePreferences.instance().firstLaunchSuccessful();
                            goToLinphoneActivity();
                        } else if (status == ConfiguringState.Failed) {
                            Toast.makeText(
                                            MenuAssistantActivity.this,
                                            getString(R.string.remote_provisioning_failure),
                                            Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                };
    }

    private void configureAccount() {
        mAccountCreator.setUsername(mUsername.getText().toString());
        mAccountCreator.setDomain(defaultDomain);
        mAccountCreator.setPassword(mPassword.getText().toString());
        //  mAccountCreator.setDisplayName(mDisplayName.getText().toString());
        mAccountCreator.setTransport(TransportType.Udp);
        //        switch (mTransport.getCheckedRadioButtonId()) {
        //            case R.id.transport_udp:
        //                mAccountCreator.setTransport(TransportType.Udp);
        //                break;
        //            case R.id.transport_tcp:
        //                mAccountCreator.setTransport(TransportType.Tcp);
        //                break;
        //            case R.id.transport_tls:
        //                mAccountCreator.setTransport(TransportType.Tls);
        //                break;
        //        }

        createProxyConfigAndLeaveAssistant();
    }

    private boolean checkCameraPermissionForQrCode() {
        int permissionGranted =
                getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
        Log.i(
                "[Permission] Manifest.permission.CAMERA is "
                        + (permissionGranted == PackageManager.PERMISSION_GRANTED
                                ? "granted"
                                : "denied"));

        if (permissionGranted != PackageManager.PERMISSION_GRANTED) {
            Log.i("[Permission] Asking for " + Manifest.permission.CAMERA);
            ActivityCompat.requestPermissions(
                    this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_RESULT);
            return false;
        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mLogin.setEnabled(!mUsername.getText().toString().isEmpty());
        //                        && !mDomain.getText().toString().isEmpty());
    }

    @Override
    public void afterTextChanged(Editable s) {}
}

            /*  TextView accountConnection = findViewById(R.id.account_connection);
                accountConnection.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(
                                        new Intent(
                                                MenuAssistantActivity.this,
                                                AccountConnectionAssistantActivity.class));
                            }
                        });
                if (getResources().getBoolean(R.bool.hide_linphone_accounts_in_assistant)) {
                    accountConnection.setVisibility(View.GONE);
                }

                TextView genericConnection = findViewById(R.id.generic_connection);
                genericConnection.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(
                                        new Intent(
                                                MenuAssistantActivity.this,
                                                GenericConnectionAssistantActivity.class));
                            }
                        });
                if (getResources().getBoolean(R.bool.hide_generic_accounts_in_assistant)) {
                    genericConnection.setVisibility(View.GONE);
                }

                TextView remoteConfiguration = findViewById(R.id.remote_configuration);
                remoteConfiguration.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(
                                        new Intent(
                                                MenuAssistantActivity.this,
                                                RemoteConfigurationAssistantActivity.class));
                            }
                        });
                if (getResources().getBoolean(R.bool.hide_remote_provisioning_in_assistant)) {
                    remoteConfiguration.setVisibility(View.GONE);
                }

                if (getResources().getBoolean(R.bool.assistant_use_linphone_login_as_first_fragment)) {
                    startActivity(
                            new Intent(
                                    MenuAssistantActivity.this, AccountConnectionAssistantActivity.class));
                    finish();
                } else if (getResources()
                        .getBoolean(R.bool.assistant_use_generic_login_as_first_fragment)) {
                    startActivity(
                            new Intent(
                                    MenuAssistantActivity.this, GenericConnectionAssistantActivity.class));
                    finish();
                } else if (getResources()
                        .getBoolean(R.bool.assistant_use_create_linphone_account_as_first_fragment)) {
                    startActivity(
                            new Intent(
                                    MenuAssistantActivity.this,
                                    PhoneAccountCreationAssistantActivity.class));
                    finish();
                }
            }

            @Override
            protected void onResume() {
                super.onResume();

                if (getResources()
                        .getBoolean(R.bool.forbid_to_leave_assistant_before_account_configuration)) {
                    mBack.setEnabled(false);
                }

                mBack.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LinphonePreferences.instance().firstLaunchSuccessful();
                                goToLinphoneActivity();
                            }
                        });
            }

            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (getResources()
                            .getBoolean(R.bool.forbid_to_leave_assistant_before_account_configuration)) {
                        // Do nothing
                        return true;
                    } else {
                        LinphonePreferences.instance().firstLaunchSuccessful();
                        goToLinphoneActivity();
                        return true;
                    }
                }
                return super.onKeyDown(keyCode, event);


                */
